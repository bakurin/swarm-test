#!/usr/bin/env bash

NODE_NAME=${1}
KS_IP=${2}
NODE_IP=$(docker-machine ip ${NODE_NAME})

docker-machine regenerate-certs -f ${NODE_NAME}
docker-machine scp -r . ${NODE_NAME}:${PROJECT_ROOT}

eval $(docker-machine env ${NODE_NAME})

docker rm -f $(docker ps -aq)

docker run -d \
    --name=swarm \
    --restart=unless-stopped \
    swarm join --addr=${NODE_IP}:2376 consul://${KS_IP}:8500

docker run -d \
    -h ${HOSTNAME} \
    --name registrator \
    --restart=unless-stopped \
    -v /var/run/docker.sock:/tmp/docker.sock \
    gliderlabs/registrator:latest -ip ${NODE_IP} consul://${KS_IP}:8500