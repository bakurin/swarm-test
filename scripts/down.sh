#!/usr/bin/env bash

export PROJECT_ROOT=/home/docker/app
DOCKER_HOST=$(docker-machine ip manager):3376
eval $(docker-machine env manager)
docker-compose stop && docker-compose down && docker-compose rm -f