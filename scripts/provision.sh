#!/usr/bin/env bash

export PROJECT_ROOT=/home/docker/app
SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE}")"; pwd -P)

# Key store
docker-machine regenerate-certs -f keystore
KS_IP=$(docker-machine ip keystore)

eval $(docker-machine env keystore)
docker rm -f $(docker ps -aq)
docker run -d \
    --name=consul-server \
    --restart=unless-stopped \
    -p ${KS_IP}:8300:8300 \
    -p ${KS_IP}:8301:8301 \
    -p ${KS_IP}:8301:8301/udp \
    -p ${KS_IP}:8302:8302 \
    -p ${KS_IP}:8302:8302/udp \
    -p ${KS_IP}:8400:8400 \
    -p ${KS_IP}:8500:8500 \
    -p ${KS_IP}:53:53 \
    -p ${KS_IP}:53:53/udp \
    -h consul \
    progrium/consul -server -bootstrap

# Http
docker-machine regenerate-certs -f http
docker-machine scp -r . http:${PROJECT_ROOT}
eval $(docker-machine env http)
docker rm -f $(docker ps -aq)

docker run -d \
    --restart=unless-stopped \
    -v "${PROJECT_ROOT}/conf/docker/nginx/nginx.conf:/etc/nginx/nginx.conf" \
    -v "${PROJECT_ROOT}/conf/docker/nginx/service.conf.ctmpl:/var/nginx/service.conf.ctmpl" \
    -p 80:80 \
     --name http \
    -e "SERVICE_NAME=http" \
    -e "SERVICE_TAGS=http" \
    -e "CONSUL_TCP_ADDR=${KS_IP}" \
    -e "CONSUL_TCP_PORT=8500" \
     bakurin/consul-template-nginx

docker run -d \
    --net=host \
    --restart=unless-stopped \
    --name registrator \
    -v /var/run/docker.sock:/tmp/docker.sock \
    gliderlabs/registrator:latest consul://${KS_IP}:8500

# Manager
docker-machine regenerate-certs -f manager
eval $(docker-machine env manager)
docker rm -f $(docker ps -aq)
docker run -d \
    --name=swarm-manager \
    --restart=unless-stopped \
    -p 3376:2375 \
    -v /var/lib/boot2docker:/certs:ro \
    swarm manage --tlsverify \
    --tlscacert=/certs/ca.pem \
    --tlscert=/certs/server.pem \
    --tlskey=/certs/server-key.pem \
    consul://${KS_IP}:8500

${SCRIPT_DIR}/provision-node.sh webapp01 ${KS_IP}
${SCRIPT_DIR}/provision-node.sh worker01 ${KS_IP}
${SCRIPT_DIR}/provision-node.sh mongodb ${KS_IP}
${SCRIPT_DIR}/provision-node.sh queue ${KS_IP}

echo -e "\n--> Swarm cluster:"
docker -H $(docker-machine ip manager):3376 info
