#!/usr/bin/env bash

export PROJECT_ROOT=/home/docker/app
eval $(docker-machine env manager)
docker-compose -H $(docker-machine ip manager):3376 up -d