#!/usr/bin/env bash

docker-machine rm -f keystore http manager webapp01 worker01 mongodb queue

docker-machine create \
    -d virtualbox \
    --virtualbox-memory "512" \
    --engine-opt="label=node.function=keystore" \
    keystore

KS_IP=$(docker-machine ip keystore)

docker-machine create \
    -d virtualbox \
    --virtualbox-memory "1024" \
    --engine-opt="label=node.function=http" \
    http

docker-machine create \
    -d virtualbox \
    --virtualbox-memory "512" \
    --engine-opt="label=node.function=manager" \
    --engine-opt="cluster-store=consul://${KS_IP}:8500" \
    --engine-opt="cluster-advertise=eth1:2376" \
    --engine-opt="dns=${KS_IP}" \
    --engine-opt="dns=8.8.8.8" \
    --engine-opt="dns-search=service.consul" \
    manager

docker-machine create \
    -d virtualbox \
    --virtualbox-memory "512" \
    --engine-opt="label=node.function=webapp" \
    --engine-opt="cluster-store=consul://${KS_IP}:8500" \
    --engine-opt="cluster-advertise=eth1:2376" \
    --engine-opt="dns=${KS_IP}" \
    --engine-opt="dns=8.8.8.8" \
    --engine-opt="dns-search=service.consul" \
    webapp01

docker-machine create \
    -d virtualbox \
    --virtualbox-memory "1024" \
    --engine-opt="label=node.function=worker" \
    --engine-opt="cluster-store=consul://${KS_IP}:8500" \
    --engine-opt="cluster-advertise=eth1:2376" \
    --engine-opt="dns=${KS_IP}" \
    --engine-opt="dns=8.8.8.8" \
    --engine-opt="dns-search=service.consul" \
    worker01

docker-machine create \
    -d virtualbox \
    --virtualbox-memory "512" \
    --engine-opt="label=node.function=mongodb" \
    --engine-opt="cluster-store=consul://${KS_IP}:8500" \
    --engine-opt="cluster-advertise=eth1:2376" \
    --engine-opt="dns=${KS_IP}" \
    --engine-opt="dns=8.8.8.8" \
    --engine-opt="dns-search=service.consul" \
    mongodb

docker-machine create \
    -d virtualbox \
    --virtualbox-memory "512" \
    --engine-opt="label=node.function=queue" \
    --engine-opt="cluster-store=consul://${KS_IP}:8500" \
    --engine-opt="cluster-advertise=eth1:2376" \
    --engine-opt="dns=${KS_IP}" \
    --engine-opt="dns=8.8.8.8" \
    --engine-opt="dns-search=service.consul" \
    queue

echo -e "\n--> Virtual machines:"
docker-machine ls

