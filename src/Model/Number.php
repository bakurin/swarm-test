<?php

namespace SwarmTest\Model;

/**
 * Class Number
 */
class Number
{
    /**
     * @var int
     */
    public $value;
    /**
     * @var int
     */
    public $factorial;
    /**
     * @var string
     */
    public $server;
    /**
     * @var string
     */
    public $worker;

    /**
     * Number constructor.
     *
     * @param $value
     * @param $server
     */
    public function __construct($value, $server)
    {
        $this->value = $value;
        $this->server = $server;
    }

    /**
     * @return array
     */
    function toArray()
    {
        return [
            'v' => $this->value,
            'f' =>$this->factorial,
            'info' => [
                'taken' => $this->server,
                'processed' => $this->worker,
            ]
        ];
    }

    /**
     * @param array $data
     *
     * @return self
     */
    public static function fromArray(array $data)
    {
        $number = new self($data['v'], $data['f']);
        $number->server = $data['info']['taken'];
        $number->worker = $data['info']['processed'];

        return $number;
    }
}