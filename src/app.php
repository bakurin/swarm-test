<?php

use MongoDB\BSON\ObjectID;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Channel\AMQPChannel;
use Silex\Application;
use SwarmTest\Model\Number;

$app = new Application();

$app['debug'] = true;

$app['collection'] = function () {
    $connection = new \MongoDB\Driver\Manager('mongodb://mongodb:27017');
    return $connection->swarm_test->numbers;
};

$app['queue'] = function () {
    $connection = new AMQPStreamConnection('rabbitmq', 5672, 'guest', 'guest');
    $channel = $connection->channel();
    $channel->queue_declare('hello', false, false, false, false);
};

$app->get('/', function () use ($app) {
    return $app->json(['ok' => true, 'message' => 'Hello!'], 200);
});

$app->get('/api/test', function () use ($app) {
    return $app->json(['ok' => true], 200);
});

$app->post('/queue', function () use ($app) {
    $int = rand(0, 10);
    $number = new Number($int, '');
    $id = new ObjectId();
    $msg = new AMQPMessage((string) $id);

    /** @var MongoCollection $collection */
    $collection = $app['collection'];
    /** @var AMQPChannel $queue */
    $queue = $app['queue'];

    $collection->insert(['_id' => $id] + $number->toArray());
    $queue->basic_publish($msg, '', 'hello');

    return $app->json(['ok' => true, 'message' => "Number ${int} has been putt into the queue!"], 201);
});

$app->run();